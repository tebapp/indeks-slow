#include <iostream>


using namespace std;

string slownik[200];
int slownikIndeks=0;

bool menu();
void dodajTekst();
void wyswietlTekst();
void usunTekst();

int main()
{
    cout << "wartosc 1 elementu: " << slownik[0] << endl;
    while(1)
        if(menu())break;
    return 0;
}

bool menu() {
    string wybor;
    cout << "Witaj w programie.\n";
    cout << "Wybierz jedna z opcji:\n1 - dodaj nowe slowo do tablicy"
         << "\n2 - wyswietl slowa z tablicy\n3 - usun slowo z tablicy"
         << "\n4 - koniec programu\nTwoj wybor: ";
    cin >> wybor;
    if(wybor.size() != 1)
        cout << "Podales zla opcje!";
    if (wybor.compare("1")==0)
        dodajTekst();
    if(wybor.compare("2")==0)
        wyswietlTekst();
    if(wybor.compare("3")==0)
        usunTekst();
    if(wybor.compare("4")==0)
        return true;
    return false;
}

void usunTekst() {
    int a =0;
    cout << "Podaj numer slowa do usuniecia: ";
    cin >> a;
    if (slownikIndeks >= a) {
        ///1 [0]
        ///2 [1]
        ///3 [2]
        ///NIE ISTNIEJE [3]
        // zakladamym, ze usuwamy 1 (czyli element 0 w tablicy)b
        //i=a
        //dzialanie for:
        //slownik[i=0]=slownik[i+1] <-2
        //slownik[i=1]=slownik[i+1] <-3
        //slownik[i=2] NIE POWINNO SIE WYKONAC!
        for(int i=a-1;i<slownikIndeks-1;i++)
            slownik[i]=slownik[i+1];
        slownik[--slownikIndeks].clear();
    }
    else
        cout << "Podana wartosc jest spoza zakresu!";
//    cout << "Ostatni element zostal wlasnie usuniety!\n";
//    if(slownikIndeks)
//        slownik[--slownikIndeks].clear();
}

void dodajTekst() {
    cout << "Podaj slowo (lub slowa), ktore maja zostac dodane"
         << " do slownika:\n";
    cin >> slownik[slownikIndeks++];
    /poniżej trzy linijki stanowią przykład powiększenia indeksu poza wywołaniem tablicy
    //jednak przez programistów nie są wykorzystywane - za dużo linii/tekstu 
//    slownikIndeks = slownikIndeks+1;
//    slownikIndeks += 1;
//    slownikIndeks++;
}

void wyswietlTekst() {
    for(int i = 0; i<slownikIndeks;i++)
        cout << i+1 << ". " << slownik[i] << endl;
    cout << endl;
}
